package az.ingress.ms9ArazSpring.repository;

import az.ingress.ms9ArazSpring.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
