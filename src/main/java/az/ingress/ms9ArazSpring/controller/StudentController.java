package az.ingress.ms9ArazSpring.controller;


import az.ingress.ms9ArazSpring.dto.StudentDto;
import az.ingress.ms9ArazSpring.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/students")
public class StudentController {

    private final StudentService studentService;

    @GetMapping(value = "/{id}")
    public StudentDto get(@PathVariable Long id) {
        return studentService.get(id);
    }

    @GetMapping("/getAllStudents")
    public List<StudentDto> getAllStudents() {
        return studentService.getAllStudents();
    }

    @PostMapping
    public StudentDto save(@RequestBody StudentDto studentDto) {
        return studentService.save(studentDto);
    }

    @PutMapping
    public StudentDto update(@RequestBody StudentDto studentDto) {
        return studentService.update(studentDto);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        studentService.delete(id);
    }

}
