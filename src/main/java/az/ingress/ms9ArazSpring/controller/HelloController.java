package az.ingress.ms9ArazSpring.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/hello")
public class HelloController {

    @GetMapping(value = "/{name}")
    public String helloUser (@PathVariable String name){
        return "Hello "+ name;
    }
}
