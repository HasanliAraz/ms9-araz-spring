package az.ingress.ms9ArazSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms9ArazSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms9ArazSpringApplication.class, args);
	}

}
