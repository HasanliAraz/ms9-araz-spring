package az.ingress.ms9ArazSpring.service;

import az.ingress.ms9ArazSpring.dto.StudentDto;

import java.util.List;

public interface StudentService {
     StudentDto get (Long id);
     StudentDto save (StudentDto studentDto);
     StudentDto update (StudentDto studentDto);
     List<StudentDto> getAllStudents ();
     void delete (Long id);
}
