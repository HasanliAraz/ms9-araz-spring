package az.ingress.ms9ArazSpring.service;


import az.ingress.ms9ArazSpring.dto.StudentDto;
import az.ingress.ms9ArazSpring.entity.Student;
import az.ingress.ms9ArazSpring.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public StudentDto get(Long id) {
        StudentDto studentDto = modelMapper.map(studentRepository.getById(id),StudentDto.class);
        return studentDto ;
    }

    @Override
    public StudentDto save(StudentDto studentDto) {
              Student save = studentRepository.save(modelMapper.map(studentDto,Student.class));
               return modelMapper.map(save,StudentDto.class);
    }

    @Override
    public StudentDto update(StudentDto studentDto) {
        studentRepository.findById(studentDto.getId()).orElseThrow((()->new RuntimeException()));
        Student student = modelMapper.map(studentDto, Student.class);
        return modelMapper.map(studentRepository.save(student) ,StudentDto.class);
    }

    @Override
    public List<StudentDto> getAllStudents() {
        List<StudentDto> studentDtos =
                modelMapper.map(studentRepository.findAll(), new TypeToken<List<StudentDto>>() {}.getType());
        return studentDtos;
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }


}
