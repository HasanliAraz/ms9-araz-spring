package az.ingress.ms9ArazSpring.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class StudentDto {
    private Long id;
    private String name;
    private String institute;
    private LocalDate birthdate;
}
